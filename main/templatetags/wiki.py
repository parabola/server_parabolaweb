from django import template
register = template.Library()

@register.simple_tag
def wiki_url(article=""):
    if article == "":
        return "https://wiki.parabola.nu/"
    else:
        return "https://wiki.parabola.nu/"+article.replace(' ', '_')
