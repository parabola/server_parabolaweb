from django import template
register = template.Library()

@register.simple_tag
def bug_link(bugid):
    return "<a href=\"https://bugs.archlinux.org/task/{0}\">FS#{0}</a>".format(bugid)
