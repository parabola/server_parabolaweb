# Where are we?
dl-cache = ../download-cache
www = $(dl-cache)/www
bin = $(dl-cache)/bin

# What versions of 3rd party libraries are we using?
jquery-ver=1.8.3
bootstrap-ver=2.1.1
tablesorter-ver=2.7
d3-ver=3.0.6
konami-ver=c0f686e647765860ff4d2fcb7b48122785432b75

# What files do we make?
branding-images = \
	sitestatic/favicon.ico sitestatic/silhouette.png \
	sitestatic/archnavbar/archlogo.png \
	sitestatic/logos/apple-touch-icon-114x114.png \
	sitestatic/logos/apple-touch-icon-144x144.png \
	sitestatic/logos/apple-touch-icon-57x57.png \
	sitestatic/logos/apple-touch-icon-72x72.png \
	sitestatic/logos/icon-transparent-64x64.png
targets = \
	$(branding-images) \
	sitestatic/.gitignore visualize/.gitignore \
	sitestatic/rss.png \
	sitestatic/rss@2x.png \
	sitestatic/bootstrap-typeahead.js \
	sitestatic/homepage.js \
	sitestatic/jquery-$(jquery-ver).min.js \
	sitestatic/jquery.tablesorter-$(tablesorter-ver).js \
	sitestatic/jquery.tablesorter-$(tablesorter-ver).min.js \
	sitestatic/konami.min.js \
	visualize/static/d3-$(d3-ver).js \
	visualize/static/d3-$(d3-ver).min.js

js-basenames = $(sort $(patsubst %.min,%,$(patsubst %.js,%,$(filter %.js,$(targets)))))
generated = $(sort $(targets) $(foreach f,$(js-basenames),$f.js $f.min.js))

generated := $(generated)
targets := $(generated)


# The base rules

all: $(targets)
.PHONY: all

clean:
	rm -f -- $(generated)
.PHONY: clean

%/.gitignore: $(MAKEFILE_LIST)
	printf -- '%s\n' $(patsubst $*%,%,$(filter $*/%,$(generated))) | LC_COLLATE=C sort > $@

# Make directories
$(dl-cache) $(dl-cache)/unzip $(bin):
	mkdir -p '$@'

# Don't have non-minimized .js stick around unless we asked for them.
.INTERMEDIATE: $(filter-out $(targets),$(generated))

# Turn on sane error handling
.DELETE_ON_ERROR:

.PHONY: FORCE


# How to download files

  mangle = $(subst %,^25,$(subst :,^3A,$(subst =,^3D,$(subst ^,^5E,$1))))
unmangle = $(subst ^5E,^,$(subst ^3D,=,$(subst ^3A,:,$(subst ^25,%,$1))))

$(www)/http/%:
	mkdir -p '$(@D)'
	wget 'http://$(call unmangle,$*)' -O '$@'
	test -f '$@' && touch '$@'

$(www)/https/%:
	mkdir -p '$(@D)'
	wget 'https://$(call unmangle,$*)' -O '$@'
	test -f '$@' && touch '$@'

$(www)/git/%: FORCE
	mkdir -p '$(@D)'
	gitget checkout 'git://$(call unmangle,$*)' '$@' || { rm -rf -- '$@'; false; }
	test -d '$@' || { rm -rf -- '$@'; false; }


# Downloaded images

$(dl-cache)/unzip/Feedicons_v.2/%: $(www)/https/web.archive.org/web/20120514074507/http^3A//www.zeusboxstudio.com/file_download/1/Feedicons_v.2.zip
	mkdir -p '$(@D)'
	bsdtar xfO $< 'Feedicons v.2/$*' > '$@'
sitestatic/rss.png: $(dl-cache)/unzip/Feedicons_v.2/RSS_16.png
	cp $< $@
sitestatic/rss@2x.png: $(dl-cache)/unzip/Feedicons_v.2/RSS_32.png
	pngcrush $< $@

$(dl-cache)/unzip/parabola-artwork: $(www)/git/projects.parabola.nu/artwork.git\#branch=official/2013 | $(dl-cache)/unzip
	cp -rT '$<' '$@'
	make -C '$@'
$(dl-cache)/unzip/parabola-artwork/%: $(dl-cache)/unzip/parabola-artwork
	test -e $@ && touch $@
$(foreach i,$(branding-images),$(eval $i: $$(dl-cache)/unzip/parabola-artwork/$(notdir $i); install -Dm644 $$< $$@))

# Non-minimized .js files
sitestatic/bootstrap-typeahead.js: sitestatic/%: $(www)/https/raw.github.com/twitter/bootstrap/v$(bootstrap-ver)/js/% Makefile.d/%.patch
	cp $< $@
	patch -i Makefile.d/$*.patch $@
sitestatic/jquery-$(jquery-ver).js: sitestatic/%: $(www)/http/code.jquery.com/%
	cp $< $@
sitestatic/jquery.tablesorter-$(tablesorter-ver).js: $(www)/https/raw.github.com/Mottie/tablesorter/v$(tablesorter-ver)/js/jquery.tablesorter.js
	cp $< $@
sitestatic/konami.js: sitestatic/%: $(www)/https/raw.github.com/snaptortoise/konami-js/$(konami-ver)/% Makefile.d/%.patch
	cp $< $@
	patch -i Makefile.d/$*.patch $@
sitestatic/homepage.js: sitestatic/bootstrap-typeahead.min.js sitestatic/konami.min.js Makefile.d/homepage.js.in
	{ \
		echo '/* bootstrap-typeahead.min.js: */' && \
		cat sitestatic/bootstrap-typeahead.min.js && \
		echo && \
		echo '/* konami.min.js: */' && \
		sed -e 's,^\s*,,' -e 's,^return.*,&;,' sitestatic/konami.min.js && \
		echo && \
		echo '/* Main homepage.js content: */' && \
		cat Makefile.d/homepage.js.in ; \
	} > $@
visualize/static/d3-$(d3-ver).js: %: $(www)/https/raw.github.com/mbostock/d3/v$(d3-ver)/d3.js
	cp $< $@


# The minimization processes

JSMIN = { sed -n '1,/\*\//p' $1; uglifyjs --mangle --compress < $1; } > $2

%.min.js: %.js
	$(call JSMIN,$<,$@)
